<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  
  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'kleuren',
        'title'       => 'Kleuren'
      )
    ),
    'settings'        => array( 
      array(
        'id'          => 'social_links_footer',
        'label'       => 'Social',
        'desc'        => 'U kunt hier social links invoeren voor in de bottom footer balk op de website. U kunt kiezen uit <code>500px</code>, <code>adn</code>, <code>amazon</code>, <code>android</code>, <code>angelist</code>, <code>apple</code>, <code>behance</code>, <code>behance-square</code>, <code>bitbucket</code>, <code>bitbucket-square</code>, <code>bitcoin</code>, <code>black-tie</code>, <code>btc</code>, <code>buysellads</code>, <code>cc-amex</code>, <code>cc-diners-club</code>, <code>cc-discover</code>, <code>cc-jcb</code>, <code>cc-mastercard</code>, <code>cc-paypal</code>, <code>cc-stripe</code>, <code>cc-visa</code>, <code>chrome</code>, <code>codepen</code>, <code>connectdevelop</code>, <code>contao</code>, <code>css3</code>, <code>dashcube</code>, <code>delicious</code>, <code>deviantart</code>, <code>digg</code>, <code>dribbble</code>, <code>dropbox</code>, <code>drupal</code>, <code>empire</code>, <code>expeditedssl</code>, <code>firefox</code>, <code>flickr</code>, <code>fonticons</code>, <code>forumbee</code>, <code>gg-circle</code>, <code>git</code>, <code>gg</code>, <code>foursquare</code>, <code>ge</code>, <code>get-pocket</code>, <code>git</code>, <code>git-square</code>, <code>github</code>, <code>github-alt</code>, <code>github-square</code>, <code>gittip</code>, <code>google</code>, <code>google-plus</code>, <code>google-plus-square</code>, <code>google-wallet</code>, <code>gratipay</code>, <code>hacker-news</code>, <code>houzz</code>, <code>html5</code>, <code>instagram</code>, <code>internet-explorer</code>, <code>ioxhost</code>, <code>joomla</code>, <code>jsfiddle</code>, <code>lastfm</code>, <code>lastfm-square</code>, <code>leanpub</code>, <code>linkedin</code>, <code>linkedin-square</code>, <code>linux</code>, <code>maxcdn</code>, <code>meanpath</code>, <code>medium</code>, <code>odnoklassniki</code>, <code>odnoklassniki-square</code>, <code>opencart</code>, <code>openid</code>, <code>opera</code>, <code>optin-monster</code>, <code>pagelines</code>, <code>paypal</code>, <code>pied-piper</code>, <code>pied-piper-square</code>, <code>pinterest</code>, <code>pinterest-p</code>, <code>pinterest-square</code>, <code>qq</code>, <code>ra</code>, <code>rebel</code>, <code>reddit</code>, <code>reddit-square</code>, <code>renren</code>, <code>safari</code>, <code>sellsy</code>, <code>share-alt</code>, <code>share-alt-square</code>, <code>shirtsinbulkt</code>, <code>simplybuilt</code>, <code>skyatlas</code>, <code>skype</code></code>, <code>slack</code>, <code>slideshare</code>, <code>soundcloud</code>, <code>spotify</code>, <code>stack-exchange</code>, <code>stack-overflow</code>, <code>steam</code>, <code>steam-square</code>, <code>stumbleupon</code>, <code>stumbleupon-circle</code>, <code>tencent-weibo</code>, <code>trello</code>, <code>tripadvisor</code>, <code>tumblr</code>, <code>tumblr-square</code>, <code>twitch</code>, <code>twitter</code>, <code>twitter-square</code>, <code>viacoin</code>, <code>vimeo</code>, <code>vimeo-square</code>, <code>vine</code>, <code>vk</code>, <code>wechat</code>, <code>weibo</code>, <code>weixin</code>, <code>whatsapp</code>, <code>wikipedia-w</code>, <code>windows</code>, <code>wordpress</code>, <code>xing</code>, <code>xing-square</code>, <code>y-combinator</code>, <code>y-combinator-square</code>, <code>yahoo</code>, <code>yc</code>, <code>yc-square</code>, <code>yelp</code>, <code>youtube</code>, <code>youtube-pay</code>, <code>youtube-square</code>',
        'std'         => '',
        'type'        => 'social-links',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'logo',
        'label'       => 'Logo',
        'desc'        => 'De logo afbeelding',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'headercolor',
        'label'       => 'Header Kleur',
        'desc'        => 'De kleur van de header',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'kleuren',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'headertextcolor',
        'label'       => 'Header Text Kleur',
        'desc'        => 'De tekst kleur van de header',
        'std'         => '',
        'type'        => 'colorpicker',
        'section'     => 'kleuren',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'previewbgcolor',
        'label'       => 'Preview Blocks',
        'desc'        => 'De achtergrond kleur en transparantie van de preview blokken',
        'std'         => '',
        'type'        => 'colorpicker-opacity',
        'section'     => 'kleuren',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      )
    )
  );
  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;
  
}